# R4INN // r4inn

# Testdata
# a <- c( 112, 128, 108, 129, 125, 153, 155, 132, 137)

# Base-R har ikke en enkel funksjon for å kalkulere kvadratsummen (Sum of Squares),
# men vi kan få det til med: sum((data - mean(data))^2):
ss <- function(x) {
  sst <- sum((x - mean(x))^2)
  return(sst)
}

# Variasjonskoeffisient har heller ikke R liggende inne:
# å bruke: sd(data) / mean(data) * 100
cv <- function(x) {
  CV <- sd(x) / mean(x) * 100
  return(CV)
}

# Standardfeil (standard error) må vi også lage selv:
se <- function(x) sd(x)/sqrt(length(x))

# Kommando for å fikse problmer med æøå
suppressWarnings({ 
  Sys.setlocale("LC_ALL","no_NB.utf8")
}) 


# Funksjonen %in% er fin å bruke når det er flere ting man skal
# filtrere eller selektere. Det er også fint å ha en funksjon som
# kan filtrere alt utenom flere ting:
'%ni%' <- Negate('%in%') # ni is short for Not In

# Pakker vi alltid bruker
list.of.packages <- c("tidyverse", "agridat")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
lapply(list.of.packages, require, character.only = TRUE)

# Hilsen
cat("Hei! \n Nå er noen hjelpefunksjoner installert: \n  ss() for kvadratsummen \n  cv() for variasjonskoeffisienten  \n  se() for standardfeil \n  %ni% som gjør det motsatte av %in%. \n \n I tillegg så er det installert noen pakker: \n  Tidyverse, som inkluderer bl.a. ggplot2 og %>% funksjonen. \n  Agridat, som inneholder masse jordbruksdata. \n \n Du skal også nå kunne lese og skrive æ, ø og å.")